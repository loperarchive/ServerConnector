package org.nocraft.loperd.serverchange.command;

import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import org.nocraft.loperd.serverchange.ServerConnectorPlugin;
import org.nocraft.loperd.serverchange.domain.Registerable;
import org.nocraft.loperd.serverchange.domain.Shutdownable;

public abstract class BaseCommand extends Command implements Registerable, Shutdownable, TabExecutor {

    private ServerConnectorPlugin plugin;

    public BaseCommand(ServerConnectorPlugin plugin, String name) {
        super(name);
        this.plugin = plugin;
    }

    @Override
    public void register() {
        this.plugin.registerCommand(this);
    }

    @Override
    public void unregister() {
        this.plugin.unregisterCommand(this);
    }

    @Override
    public void shutdown() {
        this.plugin = null;
    }
}
